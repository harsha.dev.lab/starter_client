import axios from 'axios'

export async function login (username, password) {
  const query = { username, password }

  let resp = await axios.post('http://localhost:3050/api/users/login', query).catch((err) => {
    console.error('error on login', err)
  })

  return resp.data
}

export async function register (username, password, name, email) {
  let query = { username, password, name, email }

  let resp = await axios.post('http://localhost:3050/api/users', query).catch((err) => {
    console.error('error on register', err)
  })

  return resp.data
}
