
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      { path: '', component: () => import('pages/index') },
      { path: 'login', component: () => import('pages/login') },
      { path: 'registration', component: () => import('pages/registration') }
    ]
  },
  {
    path: '/app',
    component: () => import('layouts/app'),
    children: [
      { path: 'welcome', component: () => import('pages/welcome') },
      { path: 'account', component: () => import('pages/account') },
      { path: 'nearme', component: () => import('pages/nearme') }
    ]
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
